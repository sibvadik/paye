export default HelpStrings = [
    {
        title: 'What is P.A.Y.E?',
        text: "Pay As You Earn is income tax and ACC earners' levy. It is deducted by employers from employees' salary or wages, and paid to the Inland Revenue Department (IRD) on their behalf."
    },
    {
        title: 'What is ACC?',
        text: 'Accident Compensation Corporation is the sole and compulsory provider of accident insurance in New Zealand for all work and non-work injuries. Anyone – regardless of the way in which they incurred an injury – has coverage under the Scheme.'
    },
    {
        title: 'What is KiwiSaver?',
        text: 'KiwiSaver is a voluntary long-term savings scheme. The main purpose of the KiwiSaver fund is for retirement savings, but younger participants can also use it to save a deposit for their first home.'
    },
    {
        title: 'What is IETC?',
        text: 'Independent earner tax credit is an entitlement for individuals who earn between $24,000 and $48,000 (after expenses and losses) a year. Please note threshold values are adjustable in the Advanced Settings.'
    },
    {
        title: 'Note',
        text: 'This tool should only be used to estimate your income tax. Please seek professional financial advice if you require more information regarding your taxes.'
    }
];

