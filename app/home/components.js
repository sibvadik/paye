import React, {Component} from 'react';
import {
    Container,
    Content,
    Button,
    Icon,
    Fab,
    Text,
    List,
    ListItem,
    InputGroup,
    Input,
    Grid,
    Col,
    CheckBox,
    Card,
    CardItem,
} from 'native-base';
import {View} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {roundToTwo} from '../utils'
import HomeStyles from  './styles'
import PieChart from 'react-native-pie-chart';
// import * as Animatable from 'react-native-animatable';

const ListItemInput = function (props) {
    return (
        <ListItem >
            <InputGroup disabled={props.disabled} style={props.style}>
                <Input {...props} style={{lineHeight:10}}/>
            </InputGroup>
        </ListItem>
    )
};

export class HelpListItem extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.onPress = this.onPress.bind(this);
    }

    onPress() {
        this.setState({open: !this.state.open})
    }

    render() {
        return (
            <ListItem iconRight onPress={this.onPress}>
                <Text style={{fontWeight:'bold'}}>{this.props.title}</Text>
                <Icon name={this.state.open?'md-arrow-dropup':'md-arrow-dropdown'}/>
                {this.state.open ? <Text>{this.props.text}</Text> : null}
            </ListItem>
        )
    }
}
export class NumberListItemInput extends Component {
    constructor(props) {
        super(props);
        this.onEndEditing = this.onEndEditing.bind(this);
        this.onChangeText = this.onChangeText.bind(this);
    }

    onChangeText(value) {
        let processed = parseFloat(value);
        this.props.onChangeText(isNaN(processed) ? '' : value)
    }

    onEndEditing() {
        if (this.props.value == '') {
            this.props.onChangeText(0);
        }
        else {
            this.props.onChangeText(parseFloat(this.props.value))
        }
    }

    render() {
        return (<ListItemInput value={String(this.props.value)} keyboardType="numeric" label={this.props.label}
                               inlineLabel={this.props.inlineLabel}
                               disabled={this.props.disabled}
                               style={this.props.style}
                               onChangeText={this.onChangeText}
                               onEndEditing={this.onEndEditing}/>)
    }
}
NumberListItemInput.defaultProps = {
    onEndEditing: () => {
    },
    onChangeText: () => {
    }
};

const ListItemHeading = function (props) {
    return (
        <ListItem style={HomeStyles.listItemHeading}>
            <Text style={HomeStyles.listItemHeadingText}>{props.text}</Text>
        </ListItem>
    )
};
class ResultRow extends Component {
    render() {
        let badgeStyle = {borderRadius: 0, backgroundColor: this.props.buttonColor, padding: 10};
        return (
            <View style={HomeStyles.resultsRow}>
                <Button block success large style={badgeStyle}>{this.props.name} {this.props.text}</Button>
            </View>
        )
    }
}

export class ResultList extends Component {
    render() {
        return (
            <View>
                <ResultRow buttonColor={'#B2912F'} name="Gross Pay"
                           text={roundToTwo(this.props.gross/this.props.divider)}/>
                <ResultRow buttonColor={'#F15854'} name="Take home"
                           text={roundToTwo(this.props.takeHome/this.props.divider)}/>
                <ResultRow buttonColor={'#60BD68'} name="PAYE" text={roundToTwo(this.props.paye/this.props.divider)}/>
                <ResultRow buttonColor={'#FAA43A'} name="ACC" text={roundToTwo(this.props.acc/this.props.divider)}/>
                <ResultRow buttonColor={'#5DA5DA'} name="KiwiSaver"
                           text={roundToTwo(this.props.kiwiSaver/this.props.divider)}/>
                <ResultRow buttonColor={'#DECF3F'} name="Student Loan"
                           text={roundToTwo(this.props.studentLoan/this.props.divider)}/>
                <ResultRow buttonColor={'#B276B2'} name="Total %"
                           text={roundToTwo(this.props.taxPercentage)}/>
            </View>
        )
    }
}

export class ResultsCard extends Component {
    constructor(props) {
        super(props);
        this.getChartSeries = this.getChartSeries.bind(this);
    }

    getChartSeries() {

    }

    render() {
        const chart_wh = 250;
        let resultsAttributes = ['takeHome', 'paye', 'acc', 'kiwiSaver', 'studentLoan'];
        let colorsMap = {
            takeHome: '#F15854', paye: '#60BD68', acc: '#FAA43A', kiwiSaver: '#5DA5DA', studentLoan: '#DECF3F'
        };
        let series = [];
        let colors = [];
        for (let attribute of resultsAttributes) {
            if ((attribute == 'kiwiSaver' || attribute == 'studentLoan') && this.props.results[attribute] == 0) {
                continue;
            }
            colors.push(colorsMap[attribute]);
            series.push(this.props.results[attribute] / this.props.divider)
        }
        return (
            <Card style={HomeStyles.card}>
                <CardItem>
                    <ResultList {...this.props.results} divider={this.props.divider}/>
                </CardItem>
                {parseFloat(this.props.results.gross) > 0 ?
                    <CardItem>
                        <View style={HomeStyles.pieChartContainer}>
                            <PieChart
                                chart_wh={chart_wh}
                                series={series}
                                sliceColor={colors}
                            />
                        </View>
                    </CardItem> : null}
            </Card>
        );
    }
}
export class ACCFormFields extends Component {
    render() {
        return (
            <List>
                <ListItemHeading text="ACC Settings"/>
                <NumberListItemInput inlineLabel label="Levy" value={this.props.values.accLevy}
                                     onChangeText={this.props.onLevyChange}/>
                <NumberListItemInput inlineLabel label="Income Cap" value={this.props.values.accIncomeCap}
                                     onChangeText={this.props.onIncomeCapChange}/>
            </List>
        )
    }
}
export class StudentLoanFields extends Component {
    render() {
        return (
            <List>
                <ListItemHeading text="Student Loan settings"/>
                <ListItem>
                    <CheckBox checked={this.props.values.studentLoan} onPress={this.props.onToggle}/>
                    <Text>Apply payments</Text>
                </ListItem>
            </List>
        )
    }
}

export class IetcSettingsFields extends Component {
    render() {
        return (
            <List>
                <ListItemHeading text="IETC settings"/>
                <ListItem>
                    <CheckBox checked={this.props.values.ietcEnabled} onPress={this.props.onToggle}/>
                    <Text>Apply IETC</Text>
                </ListItem>
                <NumberListItemInput inlineLabel label="Annual credit" value={this.props.values.ietcCredit}
                                     onChangeText={this.props.onCreditChange}/>
                <NumberListItemInput inlineLabel label="Min. threshold" value={this.props.values.ietcMinThreshold}
                                     onChangeText={this.props.onMinThresholdChange}/>
                <NumberListItemInput inlineLabel label="Max. threshold" value={this.props.values.ietcMaxThreshold}
                                     onChangeText={this.props.onMaxThresholdChange}/>
                <NumberListItemInput inlineLabel label="Red. point" value={this.props.values.ietcReductionPoint}
                                     onChangeText={this.props.onReductionPointChange}/>
            </List>
        )
    }
}
export class OtherSettingsFields extends Component {
    render() {
        return (
            <List>
                <ListItemHeading text="Other settings"/>
                <NumberListItemInput inlineLabel label="Hours per week" value="40"/>
            </List>
        )
    }
}

export class PayeFormFields extends Component {
    render() {
        return (
            <List>
                <ListItemHeading text="PAYE settings"/>
                <ListItem style={HomeStyles.paddingLessMarginLessComponent}>
                    <Grid>
                        <Col>
                            <Text>Income Threshold</Text>
                            <List>
                                <NumberListItemInput value={this.props.values.threshold.min[0]}
                                                     onChangeText={this.props.onMinIncomeChange}/>
                                <NumberListItemInput value={this.props.values.threshold.med[0]}
                                                     onChangeText={this.props.onMedIncomeChange}/>
                                <NumberListItemInput value={this.props.values.threshold.high[0]}
                                                     onChangeText={this.props.onHighIncomeChange}/>
                                <NumberListItemInput value={this.props.values.threshold.max[0]}
                                                     style={HomeStyles.borderLessComponent}
                                                     onChangeText={this.props.onMaxIncomeChange} disabled={true}/>
                            </List>
                        </Col>
                        <Col>
                            <Text>Tax rate</Text>
                            <List >
                                <NumberListItemInput value={String(this.props.values.threshold.min[1])}
                                                     onChangeText={this.props.onMinTaxRateChange}/>
                                <NumberListItemInput value={String(this.props.values.threshold.med[1])}
                                                     onChangeText={this.props.onMedTaxRateChange}/>
                                <NumberListItemInput value={String(this.props.values.threshold.high[1])}
                                                     onChangeText={this.props.onHighTaxRateChange}/>
                                <NumberListItemInput value={String(this.props.values.threshold.max[1])}
                                                     style={HomeStyles.borderLessComponent}
                                                     onChangeText={this.props.onMaxTaxRateChange}/>
                            </List>
                        </Col>
                    </Grid>
                </ListItem>
            </List>
        )
    }
}

export class KiwiSaverFormFields extends Component {
    constructor(props) {
        super(props);
        this.onEndEditing = this.onEndEditing.bind(this);
    }

    onEndEditing() {
        if (this.props.values.kiwiSaver == '') {
            this.props.onValueChange(0);
        }
    }

    render() {
        return (
            <List>
                <ListItemHeading text="KiwiSaver settings"/>
                <NumberListItemInput label="Contribution %" value={this.props.values.kiwiSaver}
                                     onChangeText={this.props.onValueChange} inlineLabel/>
            </List>
        )
    }
}

export class ResetForm extends Component {
    render() {
        return (
            <List>
                <ListItem>
                    <Button block danger onPress={this.props.onResetSettingsPress}>Reset settings</Button>
                </ListItem>
                <ListItem>
                    <Button block danger onPress={this.props.onClearHistoryPress}>Clear history</Button>
                </ListItem>
            </List>
        )

    }
}

const DrawerListItem = function (props) {
    return (
        <ListItem iconLeft button onPress={props.onPress}
                  style={HomeStyles.borderLessComponent}>
            <Icon name={props.icon}/>
            <Text>{props.text}</Text>
        </ListItem>
    )
};

const HistoryIcon = function (props) {
    let name = 'md-arrow-up';
    let style = HomeStyles.historyIconGreen;
    if (props.negative) {
        name = 'md-arrow-down';
        style = HomeStyles.historyIconRed;
    }
    return <Icon name={name} style={style}/>
};
export function HistoryListItem(props) {
    return (
        <ListItem onPress={props.onPress}>
            <Text>{roundToTwo(props.gross)} - {roundToTwo(props.takeHome)} - {roundToTwo(props.taxPercentage)} %
                {props.previous.dummy ? '' : <HistoryIcon negative={props.gross<props.previous.gross}/>}
            </Text>
        </ListItem>
    )
}
export class SideDrawerContent extends Component {
    constructor(props) {
        super(props);
        this.closeDrawerAndGo = this.closeDrawerAndGo.bind(this);
    }

    closeDrawerAndGo(callback) {
        this.props.closeDrawer();
        callback();
    }

    render() {
        return (
            <Container>
                <Content style={HomeStyles.drawerMargin}>
                    <List>
                        <DrawerListItem text="Basic Form" icon="md-calculator"
                                        onPress={this.closeDrawerAndGo.bind(null, Actions.home)}/>
                        <DrawerListItem text="Results" icon="md-list"
                                        onPress={this.closeDrawerAndGo.bind(null, Actions.results)}/>
                        <DrawerListItem text="History" icon="md-time"
                                        onPress={this.closeDrawerAndGo.bind(null, Actions.history)}/>
                        <DrawerListItem text="Advanced settings" icon="md-settings"
                                        onPress={this.closeDrawerAndGo.bind(null, Actions.advanced)}/>
                        <DrawerListItem text="Help" icon="md-help-circle"
                                        onPress={this.closeDrawerAndGo.bind(null, Actions.help)}/>
                        {/*<DrawerListItem text="Contact" icon="md-happy"*/}
                        {/*onPress={this.closeDrawerAndGo.bind(null, Actions.advanced)}/>*/}
                        <DrawerListItem text="About" icon="md-information-circle"
                                        onPress={this.closeDrawerAndGo.bind(null, Actions.about)}/>
                    </List>
                </Content>
            </Container>
        )
    }
}