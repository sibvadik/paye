import constants from './constants'
export default {
    onTotalIncomeChange: function (newValue) {
        return {type: constants.TOTAL_INCOME_CHANGE, data: newValue}
    },
    onKiwiSaverToggle: function () {
        return {type: constants.KIWI_SAVER_TOGGLE};
    },
    onKiwiSaverPercentageChange: function (newValue) {
        return {type: constants.KIWI_SAVER_PERCENTAGE_CHANGE, data: newValue};
    },
    onStudentLoanToggle: function () {
        return {type: constants.STUDENT_LOAN_TOGGLE};
    },
    onACCLevyChange: function (newValue) {
        return {type: constants.ACC_LEVY_CHANGE, data: newValue};
    },
    onACCIncomeCapChange: function (newValue) {
        return {type: constants.ACC_INCOME_CAP_CHANGE, data: newValue};
    },
    onThresholdMinIncomeChange: function (newValue) {
        return {type: constants.THRESHOLD_MIN_INCOME_CHANGE, data: newValue};
    },
    onThresholdMedIncomeChange: function (newValue) {
        return {type: constants.THRESHOLD_MED_INCOME_CHANGE, data: newValue};
    },
    onThresholdHighIncomeChange: function (newValue) {
        return {type: constants.THRESHOLD_HIGH_INCOME_CHANGE, data: newValue};
    },
    onThresholdMinTaxRateChange: function (newValue) {
        return {type: constants.THRESHOLD_MIN_TAX_CHANGE, data: newValue};
    },
    onThresholdMedTaxRateChange: function (newValue) {
        return {type: constants.THRESHOLD_MED_TAX_CHANGE, data: newValue};
    },
    onThresholdHighTaxRateChange: function (newValue) {
        return {type: constants.THRESHOLD_HIGH_TAX_CHANGE, data: newValue};
    },
    onThresholdMaxTaxRateChange: function (newValue) {
        return {type: constants.THRESHOLD_MAX_TAX_CHANGE, data: newValue};
    },
    onIetcToggle: function () {
        return {type: constants.IETC_ENABLED_TOGGLE};
    },
    onIetcCreditChange: function (newValue) {
        return {type: constants.IETC_CREDIT_CHANGE, data:newValue};
    },
    onIetcMinThresholdChange: function (newValue) {
        return {type: constants.IETC_MIN_THRESHOLD_CHANGE, data: newValue}
    },
    onIetcMaxThresholdChange: function (newValue) {
        return {type: constants.IETC_MAX_THRESHOLD_CHANGE, data: newValue}
    },
    onIetcReductionPointChange: function (newValue) {
        return {type: constants.IETC_REDUCTION_POINT_CHANGE, data: newValue}
    },
    postContactForm: function (data) {

    },
    clearHistory: function () {
        return {type: constants.CLEAR_HISTORY};
    },
    resetSettings: function () {
        return {type: constants.RESET_SETTINGS};
    },
    addToHistory: function (historyItem) {
        return {type: constants.ADD_TO_HISTORY, data: historyItem}
    },
    dismissRateCard: function () {
        return {type: constants.DISMISS_RATE_CARD};
    }
}
