import React, {Component} from 'react';
import {
    Container,
    Content,
    Button,
    Icon,
    Text,
    ListItem,
    CheckBox,
    List,
    Tabs,
    Card, CardItem,
} from 'native-base';
import {Actions, DefaultRenderer} from 'react-native-router-flux';
import {
    SideDrawerContent,
    HelpListItem,
    OtherSettingsFields,
    KiwiSaverFormFields,
    PayeFormFields,
    IetcSettingsFields,
    ResetForm,
    StudentLoanFields,
    ACCFormFields,
    HistoryListItem,
    NumberListItemInput,
    ResultsCard
} from './components';
import {View, ScrollView, Alert} from 'react-native';
import Drawer from 'react-native-drawer'
import {calculateAllTaxes} from '../utils'
import HomeStyles from  './styles'
import HelpStrings from './help_strings'

const calculatePaye = function (values) {
    return calculateAllTaxes(
        values.totalIncome,
        values.threshold,
        values.kiwiSaverEnabled ? values.kiwiSaver : 0,
        values.accLevy,
        values.accIncomeCap,
        values.hoursWorked,
        values.studentLoan,
        values.ietcEnabled,
        values.ietcCredit,
        values.ietcMinThreshold,
        values.ietcMaxThreshold,
        values.ietcReductionPoint
    );
};

export class ResultsContainer extends Component {
    render() {
        let results = calculatePaye(this.props.home.values);
        let showPieChart = parseFloat(results.gross) > 0;
        return (
            <Container style={HomeStyles.globalMargin}>
                <Content>
                    <Tabs>
                        <Content tabLabel="Hour">
                            <ResultsCard results={results} divider={52*40} showPieChart={showPieChart}/>
                        </Content>
                        <Content tabLabel="Week">
                            <ResultsCard results={results} divider={52} showPieChart={showPieChart}/>
                        </Content>
                        <Content tabLabel="Fortnight">
                            <ResultsCard results={results} divider={26} showPieChart={showPieChart}/>
                        </Content>
                        <Content tabLabel="Month">
                            <ResultsCard results={results} divider={12} showPieChart={showPieChart}/>
                        </Content>
                        <Content tabLabel="Year">
                            <ResultsCard results={results} divider={1} showPieChart={showPieChart}/>
                        </Content>
                    </Tabs>
                </Content>
            </Container>)
    }
}

export class AdvancedSettingsFormContainer extends Component {
    constructor(props) {
        super(props);
        this.onResetSettingsPress = this.onResetSettingsPress.bind(this);
        this.onClearHistoryPress = this.onClearHistoryPress.bind(this);
    }

    onResetSettingsPress() {
        Alert.alert(
            'Reset settings',
            'Are you sure you want to reset settings?',
            [
                {text: 'Yes', onPress: () => this.props.resetSettings()},
                {text: 'No', onPress: () => console.log('Nope')},
            ]
        );
    };

    onClearHistoryPress() {
        Alert.alert(
            'Clear history',
            'Are you sure you want to clear history?',
            [
                {text: 'Yes', onPress: () => this.props.clearHistory()},
                {text: 'No', onPress: () => console.log('Nope')},
            ]
        );
    };

    render() {
        return (
            <Container style={HomeStyles.globalMargin}>
                <Content>
                    <ScrollView>
                        {/*<OtherSettingsFields values={this.props.home.values}/>*/}
                        <KiwiSaverFormFields values={this.props.home.values}
                                             onValueChange={this.props.onKiwiSaverPercentageChange}/>
                        <PayeFormFields values={this.props.home.values}
                                        onMinIncomeChange={this.props.onThresholdMinIncomeChange}
                                        onMedIncomeChange={this.props.onThresholdMedIncomeChange}
                                        onHighIncomeChange={this.props.onThresholdHighIncomeChange}
                                        onMaxIncomeChange={this.props.onThresholdMaxIncomeChange}
                                        onMinTaxRateChange={this.props.onThresholdMinTaxRateChange}
                                        onMedTaxRateChange={this.props.onThresholdMedTaxRateChange}
                                        onHighTaxRateChange={this.props.onThresholdHighTaxRateChange}
                                        onMaxTaxRateChange={this.props.onThresholdMaxTaxRateChange}/>
                        <StudentLoanFields values={this.props.home.values}
                                           onToggle={this.props.onStudentLoanToggle}/>
                        <ACCFormFields values={this.props.home.values}
                                       onLevyChange={this.props.onACCLevyChange}
                                       onIncomeCapChange={this.props.onACCIncomeCapChange}/>
                        <IetcSettingsFields values={this.props.home.values}
                                            onToggle={this.props.onIetcToggle}
                                            onCreditChange={this.props.onIetcCreditChange}
                                            onMinThresholdChange={this.props.onIetcMinThresholdChange}
                                            onMaxThresholdChange={this.props.onIetcMaxThresholdChange}
                                            onReductionPointChange={this.props.onIetcReductionPointChange}/>
                        <ResetForm onResetSettingsPress={this.onResetSettingsPress}
                                   onClearHistoryPress={this.onClearHistoryPress}/>
                    </ScrollView>
                </Content>
            </Container>
        )
    }
}


export class HomeContainer extends Component {
    constructor(props) {
        super(props);
        this.calculateOnPress = this.calculateOnPress.bind(this);
        this.renderRateCard = this.renderRateCard.bind(this);
        this.dismissRateCard = this.dismissRateCard.bind(this);
    }

    calculateOnPress() {
        // dont like this...
        let results = calculatePaye(this.props.home.values);
        this.props.addToHistory({
            gross: results.gross,
            takeHome: results.takeHome,
            taxPercentage: results.taxPercentage
        });
        Actions.results();
    }

    advancedButtonOnPress() {
        Actions.advanced();
    }

    renderRateCard() {
        if (this.props.home.history.length > 2 && !this.props.home.rateCardDismissed) {
            return (
                <Card style={HomeStyles.card}>
                    <CardItem header>
                        <Text>
                            Rating
                        </Text>
                    </CardItem>
                    <CardItem>
                        <Text>
                            Please consider rating this app
                        </Text>
                        <Button onPress={this.dismissRateCard} small warning block
                                style={HomeStyles.cardButton}>Dismiss</Button>
                    </CardItem>
                </Card>
            )
        }
        return null;
    }

    dismissRateCard() {
        this.props.dismissRateCard()
    }

    render() {
        return (
            <Container style={HomeStyles.globalMargin}>
                <Content>
                    <Card style={HomeStyles.card}>
                        <CardItem>
                            <List>
                                <NumberListItemInput label="Total income" inlineLabel
                                                     onChangeText={this.props.onTotalIncomeChange}
                                                     value={this.props.home.values.totalIncome}/>
                                <ListItem>
                                    <CheckBox checked={this.props.home.values.kiwiSaverEnabled}
                                              onPress={this.props.onKiwiSaverToggle}/>
                                    <Text>Kiwi Saver</Text>
                                </ListItem>
                                <ListItem>
                                    <CheckBox checked={this.props.home.values.studentLoan}
                                              onPress={this.props.onStudentLoanToggle}/>
                                    <Text>Student loan</Text>
                                </ListItem>
                            </List>
                            <Button style={HomeStyles.cardButton} block success onPress={this.calculateOnPress}>
                                <Icon name='ios-calculator'/> Calculate
                            </Button>
                        </CardItem>
                    </Card>
                    <Card style={HomeStyles.card}>
                        <CardItem header>
                            <Text>
                                Did you know?
                            </Text>
                        </CardItem>
                        <CardItem>
                            <Text>You can customise various tax settings by using</Text>
                            <Button onPress={this.advancedButtonOnPress} small info block style={HomeStyles.cardButton}>Advanced Settings</Button>
                        </CardItem>
                    </Card>
                    {this.renderRateCard()}
                </Content>
            </Container>
        )
    }
}

export class HistoryContainer extends Component {
    constructor(props) {
        super(props);
        this.renderRows = this.renderRows.bind(this);
        this.onRowPress = this.onRowPress.bind(this);
    }

    onRowPress(gross) {
        this.props.onTotalIncomeChange(String(gross));
        Actions.results();
    }

    renderRows() {
        let rows = [];
        let previousItem = {dummy: true};
        let self = this;
        this.props.home.history.forEach(function (item, index) {
            rows.push(<HistoryListItem onPress={self.onRowPress.bind(null,item.gross)} key={index} gross={item.gross}
                                       taxPercentage={item.taxPercentage} takeHome={item.takeHome}
                                       previous={previousItem}/>);
            previousItem = item;
        });
        return rows.reverse();
    }

    render() {
        let rows = this.renderRows();
        return (
            <Container style={HomeStyles.globalMargin}>
                <Content>
                    <List>
                        {rows.length > 0 ? rows : <ListItem><Text>Nothing here yet.</Text></ListItem>}
                    </List>
                </Content>
            </Container>
        )
    }
}

export class AboutContainer extends Component {
    render() {
        return (
            <Container >
                <Content>
                    <View style={HomeStyles.aboutPage}>
                        <Icon name="md-calculator" style={HomeStyles.aboutIcon}/>
                        <Text>NZ PAYE Calculator</Text>
                        <Text>Version 1.3.0</Text>
                        <Text>Created by Vadim Chernov</Text>
                    </View>
                </Content>
            </Container>
        )
    }
}

export class HelpContainer extends Component {
    constructor(props) {
        super(props);
        this.renderHelpRows = this.renderHelpRows.bind(this);
    }

    renderHelpRows() {
        let helpListItems = [];
        HelpStrings.forEach(function (item,index) {
            helpListItems.push(<HelpListItem key={index} title={item.title} text={item.text}/>)
        });
        return helpListItems;
    }

    render() {
        return (
            <Container style={HomeStyles.globalMargin}>
                <Content>
                    <List>
                        {this.renderHelpRows()}
                    </List>
                </Content>
            </Container>
        )
    }
}
export class SideDrawerContainer extends Component {
    constructor(props) {
        super(props);
        this.close = this.close.bind(this);
    }

    close() {
        this._drawer.close()
    }

    render() {

        const state = this.props.navigationState;
        const children = state.children;
        return (
            <Drawer
                ref={(ref) => this._drawer = ref}
                open={state.open}
                onOpen={()=>Actions.refresh({key:state.key, open: true})}
                onClose={()=>Actions.refresh({key:state.key, open: false})}
                type="displace"
                content={<SideDrawerContent closeDrawer={this.close}/>}
                tapToClose={true}
                openDrawerOffset={0.2}
                panCloseMask={0.2}
                negotiatePan={true}
                tweenHandler={(ratio) => ({main: { opacity:Math.max(0.54,1-ratio) }})}
            >
                <DefaultRenderer navigationState={children[0]} onNavigate={this.props.onNavigate}/>
            </Drawer>
        );
    }
}
