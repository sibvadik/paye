import {StyleSheet} from 'react-native';

export default HomeStyles = StyleSheet.create({
    globalMargin: {
        marginTop: 50
    },
    drawerMargin: {
        marginTop: 0
    },
    resultsRow: {
        marginBottom: 20,
    },
    borderLessComponent: {
        borderWidth: 0,
        borderColor: "#ffffff"
    },
    historyIconRed: {
        fontSize: 20,
        marginTop: 8,
        color: 'red'
    },
    historyIconGreen: {
        fontSize: 18,
        marginTop: 10,
        color: 'green'
    },
    aboutPage: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 140
    },
    aboutIcon: {
        fontSize: 70,
    },
    submitButton: {
        marginTop: 20, marginBottom: 20, paddingHorizontal: 60, marginLeft: 10, marginRight: 10
    },
    listItemHeadingText: {
        fontWeight: 'bold',
    },
    card: {
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10
    },
    cardButton: {
        marginTop: 10,
    },
    pieChartContainer: {
        flex: 1,
        alignItems: 'center'
    },
    listItemHeading: {borderColor: '#ffffff', paddingTop: 7, paddingBottom: 0, marginBottom: -3},
    paddingLessMarginLessComponent: {marginBottom: 0, paddingBottom: 0}
});