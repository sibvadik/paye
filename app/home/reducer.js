import constants from './constants'
import initialState from './../initial_state'
export default function HomeReducer(state, action) {
    let newState = Object.assign({}, state);
    switch (action.type) {
        case(constants.TOTAL_INCOME_CHANGE):
            newState.values.totalIncome = action.data;
            return newState;
        case(constants.KIWI_SAVER_TOGGLE):
            newState.values.kiwiSaverEnabled = !state.values.kiwiSaverEnabled;
            return newState;
        case(constants.KIWI_SAVER_PERCENTAGE_CHANGE):
            newState.values.kiwiSaver = action.data;
            return newState;
        case(constants.STUDENT_LOAN_TOGGLE):
            newState.values.studentLoan = !state.values.studentLoan;
            return newState;
        case(constants.ACC_LEVY_CHANGE):
            newState.values.accLevy = action.data;
            return newState;
        case(constants.ACC_INCOME_CAP_CHANGE):
            newState.values.accIncomeCap = action.data;
            return newState;
        case(constants.THRESHOLD_MIN_INCOME_CHANGE):
            newState.values.threshold.min[0] = action.data;
            return newState;
        case(constants.THRESHOLD_MED_INCOME_CHANGE):
            newState.values.threshold.med[0] = action.data;
            return newState;
        case(constants.THRESHOLD_HIGH_INCOME_CHANGE):
            newState.values.threshold.high[0] = action.data;
            let parsed = parseFloat(action.data);
            if (parsed != NaN) {
                newState.values.threshold.max[0] = parsed + 1;
            }
            return newState;
        case(constants.THRESHOLD_MAX_INCOME_CHANGE):
            newState.values.threshold.max[0] = action.data;
            return newState;
        case(constants.THRESHOLD_MIN_TAX_CHANGE):
            newState.values.threshold.min[1] = action.data;
            return newState;
        case(constants.THRESHOLD_MED_TAX_CHANGE):
            newState.values.threshold.med[1] = action.data;
            return newState;
        case(constants.THRESHOLD_HIGH_TAX_CHANGE):
            newState.values.threshold.high[1] = action.data;
            return newState;
        case(constants.THRESHOLD_MAX_TAX_CHANGE):
            newState.values.threshold.max[1] = action.data;
            return newState;
        case(constants.IETC_ENABLED_TOGGLE):
            newState.values.ietcEnabled = !newState.values.ietcEnabled;
            return newState;
        case(constants.IETC_CREDIT_CHANGE):
            newState.values.ietcCredit = action.data;
            return newState;
        case(constants.IETC_MIN_THRESHOLD_CHANGE):
            newState.values.ietcMinThreshold = action.data;
            return newState;
        case(constants.IETC_MAX_THRESHOLD_CHANGE):
            newState.values.ietcMaxThreshold = action.data;
            return newState;
        case(constants.IETC_REDUCTION_POINT_CHANGE):
            newState.values.ietcReductionPoint = action.data;
            return newState;
        case(constants.CLEAR_HISTORY):
            newState.history = [];
            return newState;
        case(constants.RESET_SETTINGS):
            newState.values = initialState().home.values;
            return newState;
        case(constants.ADD_TO_HISTORY):
            if (!newState.history.some((item) => {
                    return String(item.gross) == String(action.data.gross)
                })) {
                newState.history.push(action.data)
            }
            return newState;
        case(constants.DISMISS_RATE_CARD):
            newState.rateCardDismissed = true;
            return newState;
        default:
            return state || initialState;
    }
}