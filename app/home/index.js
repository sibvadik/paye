import React from 'react';
import {
    HomeContainer,
    ResultsContainer,
    AdvancedSettingsFormContainer,
    SideDrawerContainer,
    AboutContainer,
    HistoryContainer,
    HelpContainer
} from './containers'
import {connect} from 'react-redux';
import actions from './actions';

let mapStateToProps = function (state) {
    return {home: state.home}
};
let mapDispatchToProps = function (dispatch, getState) {
    return {
        onTotalIncomeChange: function (newValue) {
            dispatch(actions.onTotalIncomeChange(newValue));
        },
        onKiwiSaverPercentageChange: function (newValue) {
            dispatch(actions.onKiwiSaverPercentageChange(newValue));
        },
        onKiwiSaverToggle: function () {
            dispatch(actions.onKiwiSaverToggle())
        },
        onStudentLoanToggle: function (newValue) {
            dispatch(actions.onStudentLoanToggle(newValue))
        },
        onACCLevyChange: function (newValue) {
            dispatch(actions.onACCLevyChange(newValue))
        },
        onACCIncomeCapChange: function (newValue) {
            dispatch(actions.onACCIncomeCapChange(newValue))
        },
        onThresholdMinIncomeChange: function (newValue) {
            dispatch(actions.onThresholdMinIncomeChange(newValue))
        },
        onThresholdMedIncomeChange: function (newValue) {
            dispatch(actions.onThresholdMedIncomeChange(newValue))
        },
        onThresholdHighIncomeChange: function (newValue) {
            dispatch(actions.onThresholdHighIncomeChange(newValue))
        },
        onThresholdMaxIncomeChange: function (newValue) {
            dispatch(actions.onThresholdMaxIncomeChange(newValue))
        },
        onThresholdMinTaxRateChange: function (newValue) {
            dispatch(actions.onThresholdMinTaxRateChange(newValue))
        },
        onThresholdMedTaxRateChange: function (newValue) {
            dispatch(actions.onThresholdMedTaxRateChange(newValue))
        },
        onThresholdHighTaxRateChange: function (newValue) {
            dispatch(actions.onThresholdHighTaxRateChange(newValue))
        },
        onThresholdMaxTaxRateChange: function (newValue) {
            dispatch(actions.onThresholdMaxTaxRateChange(newValue))
        },
        onIetcToggle:function () {
            dispatch(actions.onIetcToggle())
        },
        onIetcCreditChange:function (newValue) {
            dispatch(actions.onIetcCreditChange(newValue))
        },
        onIetcMinThresholdChange:function(newValue){
            dispatch(actions.onIetcMinThresholdChange(newValue))
        },
        onIetcMaxThresholdChange:function(newValue){
            dispatch(actions.onIetcMaxThresholdChange(newValue))
        },
        onIetcReductionPointChange:function (newValue) {
            dispatch(actions.ietcReductionPointChange(newValue))
        },
        postContactForm: function (data) {
            dispatch(actions.postContactForm(data))
        },
        clearHistory: function () {
            dispatch(actions.clearHistory())
        },
        resetSettings: function () {
            dispatch(actions.resetSettings())
        },
        addToHistory: function (historyItem) {
            dispatch(actions.addToHistory(historyItem))
        },
        dismissRateCard: function () {
            dispatch(actions.dismissRateCard())
        }
    }
};

module.exports = {
    HomePage: connect(mapStateToProps, mapDispatchToProps)(HomeContainer),
    ResultsPage: connect(mapStateToProps, mapDispatchToProps)(ResultsContainer),
    AdvancedSettingsFormPage: connect(mapStateToProps, mapDispatchToProps)(AdvancedSettingsFormContainer),
    HistoryPage: connect(mapStateToProps, mapDispatchToProps)(HistoryContainer),
    SideDrawer: connect(mapStateToProps, mapDispatchToProps)(SideDrawerContainer),
    AboutPage: AboutContainer,
    HelpPage: HelpContainer
};