export default function initialState() {
    return {
        home: {
            values: {
                totalIncome: 45000,
                accLevy: 1.39,
                accIncomeCap: 122063,
                hoursWorked: 40,
                kiwiSaverEnabled: true,
                kiwiSaver: 3,
                studentLoan: false,
                threshold: {
                    min: [14000, 10.5],
                    med: [48000, 17.5],
                    high: [70000, 30],
                    max: [70001, 33]
                },
                ietcEnabled: true,
                ietcMinThreshold: 24000,
                ietcMaxThreshold: 48000,
                ietcReductionPoint: 44000,
                ietcCredit: 520
            },
            history: [],
            rateCardDismissed: false
        },

    }
};