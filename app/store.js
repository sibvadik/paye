import {createStore, applyMiddleware, compose} from 'redux';
import * as storage from 'redux-storage'
import thunk from 'redux-thunk';
import initialState from './initial_state'
import rootReducer from './root_reducer';
import createEngine from 'redux-storage-engine-reactnativeasyncstorage';


const reducer = storage.reducer(rootReducer);
const engine = createEngine('paye');
const storageMiddleware = storage.createMiddleware(engine);
const load = storage.createLoader(engine);
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default store = createStore(reducer, initialState(), composeEnhancers(
    applyMiddleware(thunk),
    applyMiddleware(storageMiddleware),
));
load(store);