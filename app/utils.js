const calculateFullTax = function (previousThreshold, threshold) {
    return (threshold[0] - previousThreshold[0]) * threshold[1] / 100
};

const getKiwiSaver = function (income, kiwiSaverPercentage) {
    return (income * kiwiSaverPercentage / 100)
};
const getAcc = function (income, levyPercentage, levyMax) {
    if (income >= levyMax) {
        return levyMax * levyPercentage / 100;
    }
    return (income * levyPercentage / 100)
};
const dummyThreshold = [0];

const getPaye = function (income, threshold) {
    if (income >= threshold.high[0]) {
        return (calculateFullTax(dummyThreshold, threshold.min) + calculateFullTax(threshold.min, threshold.med) + calculateFullTax(threshold.med, threshold.high) + (income - threshold.high[0]) * threshold.max[1] / 100)
    } else if (income >= threshold.med[0]) {
        return (calculateFullTax(dummyThreshold, threshold.min) + calculateFullTax(threshold.min, threshold.med) + ((income - threshold.med[0]) * threshold.high[1] / 100));
    } else if (income >= threshold.min[0]) {
        return (calculateFullTax(dummyThreshold, threshold.min) + ((income - threshold.min[0]) * threshold.med[1] / 100))
    } else {
        return income * threshold.min[1] / 100
    }
};

const getStudentLoan = function (income) {
    // 12% of everything you make over $19,084.
    let minIncome = 19084;
    let rate = 12;
    if (income >= minIncome) {
        return (income - minIncome) * rate / 100;
    }
    return income;
};
const getIetcEntitlement = function (income, annualCredit, minThreshold, maxThreshold, reductionPoint,) {
    if (income >= minThreshold  && income <= reductionPoint) {
        return annualCredit;
    }
    const reductionRate = 0.13;
    return annualCredit - (income - reductionPoint) * reductionRate;
};

export function roundToTwo(floatNum) {
    return parseFloat(Math.round(floatNum * 100) / 100).toFixed(2);
}

export function calculateAllTaxes(income, threshold, kiwiSaverPercentage, levyPercentage, levyMax,
                                  hoursWorked, applyStudentLoan, ietcEnabled, ietcCredit, ietcMinThreshold,
                                  ietcMaxThreshold, ietcReductionPoint) {
    let acc = getAcc(income, levyPercentage, levyMax);
    let paye = getPaye(income, threshold);
    let kiwisaver = getKiwiSaver(income, kiwiSaverPercentage);
    let studentLoan = applyStudentLoan ? getStudentLoan(income) : 0;
    let takeHome = income - acc - paye - kiwisaver - studentLoan;
    let ietcEntitlement = (income < ietcMaxThreshold) && ietcEnabled ?
        getIetcEntitlement(income, ietcCredit, ietcMinThreshold, ietcMaxThreshold, ietcReductionPoint) : 0;
    return {
        acc: acc,
        paye: paye,
        kiwiSaver: kiwisaver,
        studentLoan: studentLoan,
        takeHome: takeHome,
        gross: income,
        taxPercentage: 100 - (takeHome * 100 / income),
        ietc: ietcEntitlement
    }
}