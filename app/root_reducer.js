import {combineReducers} from 'redux';
import HomeReducer  from './home/reducer';
export default  rootReducer = combineReducers({home: HomeReducer});