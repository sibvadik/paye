import React, {Component} from 'react';
import {Router, Scene} from 'react-native-router-flux';
import {HomePage, ResultsPage, AdvancedSettingsFormPage, SideDrawer, AboutPage, HistoryPage, HelpPage} from './home';
import {Provider} from 'react-redux';
import store from './store';
import {View, Text} from 'react-native';


export default function App(props) {
    return (
        <Provider store={store}>
            <Router>
                <Scene key="root" component={SideDrawer} open={false}>
                    <Scene key="main" tabs={true}>
                        <Scene key="home" component={HomePage} title="NZ PAYE calculator" initial={true}/>
                        <Scene key="results" component={ResultsPage} title="Results"/>
                        <Scene key="advanced" component={AdvancedSettingsFormPage} title="Advanced settings"/>
                        <Scene key="history" component={HistoryPage} title="History" />
                        <Scene key="help" component={HelpPage} title="Help"/>
                        <Scene key="about" component={AboutPage} title="About"/>
                    </Scene>
                </Scene>
            </Router>
        </Provider>
    )
}